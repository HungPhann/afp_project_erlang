-module(sat_test).
-export([prop_length_sat/0,prop_check_result_sat/0]).

-include_lib("proper/include/proper.hrl").
-include_lib("eunit/include/eunit.hrl").


%%%===================================================================
%%% Manual Test
%%%===================================================================
sat_test_() -> 
    [{timeout, 10000, ?_assertMatch({sat, _}, solve_sat([]))},
    {timeout, 10000, ?_assertEqual(true, validate_clause([], element(2, solve_sat([]))))},

    {timeout, 10000, ?_assertMatch({sat, _}, solve_sat([{-13,6,7},{-8,-9,-1},{-17,-10,-6},{7,2,6},{8,-3,-10},{-16,20,17},{9,8,-18},{13,10,-11},{-14,16,5},{-16,9,-10},{-3,18,-14},{18,16,-10},{-10,-2,-11},{5,7,-16},{-7,-18,13},{-7,-4,-15},{-14,7,-19},{19,-4,-6},{18,20,-10},{-15,18,16}]))},
    {timeout, 10000, ?_assertEqual(true, validate_clause([{-13,6,7},{-8,-9,-1},{-17,-10,-6},{7,2,6},{8,-3,-10},{-16,20,17},{9,8,-18},{13,10,-11},{-14,16,5},{-16,9,-10},{-3,18,-14},{18,16,-10},{-10,-2,-11},{5,7,-16},{-7,-18,13},{-7,-4,-15},{-14,7,-19},{19,-4,-6},{18,20,-10},{-15,18,16}], element(2, solve_sat([{-13,6,7},{-8,-9,-1},{-17,-10,-6},{7,2,6},{8,-3,-10},{-16,20,17},{9,8,-18},{13,10,-11},{-14,16,5},{-16,9,-10},{-3,18,-14},{18,16,-10},{-10,-2,-11},{5,7,-16},{-7,-18,13},{-7,-4,-15},{-14,7,-19},{19,-4,-6},{18,20,-10},{-15,18,16}]))))},

    {timeout, 10000, ?_assertMatch({sat, _}, solve_sat([{-1,-2,-5},{5,1,-2},{3,-5,4},{-3,-1,-4},{-4,2,3},{-4,-5,-2},{-2,4,-3},{-1,5,3},{-1,-3,-2},{4,-1,2},{5,4,3},{4,1,5},{1,-5,-2},{-1,-2,4},{-1,4,3},{4,5,-2},{2,3,4},{3,2,-5},{-3,-2,-4},{-2,-1,4}]))},
    {timeout, 10000, ?_assertEqual(true, validate_clause([{-1,-2,-5},{5,1,-2},{3,-5,4},{-3,-1,-4},{-4,2,3},{-4,-5,-2},{-2,4,-3},{-1,5,3},{-1,-3,-2},{4,-1,2},{5,4,3},{4,1,5},{1,-5,-2},{-1,-2,4},{-1,4,3},{4,5,-2},{2,3,4},{3,2,-5},{-3,-2,-4},{-2,-1,4}], element(2, solve_sat([{-1,-2,-5},{5,1,-2},{3,-5,4},{-3,-1,-4},{-4,2,3},{-4,-5,-2},{-2,4,-3},{-1,5,3},{-1,-3,-2},{4,-1,2},{5,4,3},{4,1,5},{1,-5,-2},{-1,-2,4},{-1,4,3},{4,5,-2},{2,3,4},{3,2,-5},{-3,-2,-4},{-2,-1,4}]))))},
    
    {timeout, 10000, ?_assertMatch({sat, _}, solve_sat([{-1,-15,20},{11,10,9},{-12,-5,16},{-2,14,-15},{-9,-11,14},{-18,12,2},{12,-6,20},{8,-12,-16},{17,-15,1},{-14,6,10},{13,15,14},{-6,-9,-5},{12,14,17},{8,19,3},{1,-8,-19},{-11,-2,1},{-1,-19,8},{-2,-1,-7},{-16,20,13},{-20,-19,4},{-9,-16,1},{7,-1,-6},{10,11,-5},{6,-15,7},{7,-13,10},{18,8,-13},{8,11,-19},{-6,18,-5},{-6,16,-12},{-20,-14,-1},{4,10,1},{-15,14,7},{-15,4,-18},{8,3,-17},{13,-3,1},{4,-13,-7},{16,-10,-5},{3,-1,-6},{-20,8,-18},{-9,-15,7},{2,-1,-8},{-12,20,-6},{-18,11,13},{4,-20,13},{-1,6,14},{10,3,-18},{3,13,7},{-1,-17,9},{16,-9,4},{13,2,-9}]))},
    {timeout, 10000, ?_assertEqual(true, validate_clause([{-1,-15,20},{11,10,9},{-12,-5,16},{-2,14,-15},{-9,-11,14},{-18,12,2},{12,-6,20},{8,-12,-16},{17,-15,1},{-14,6,10},{13,15,14},{-6,-9,-5},{12,14,17},{8,19,3},{1,-8,-19},{-11,-2,1},{-1,-19,8},{-2,-1,-7},{-16,20,13},{-20,-19,4},{-9,-16,1},{7,-1,-6},{10,11,-5},{6,-15,7},{7,-13,10},{18,8,-13},{8,11,-19},{-6,18,-5},{-6,16,-12},{-20,-14,-1},{4,10,1},{-15,14,7},{-15,4,-18},{8,3,-17},{13,-3,1},{4,-13,-7},{16,-10,-5},{3,-1,-6},{-20,8,-18},{-9,-15,7},{2,-1,-8},{-12,20,-6},{-18,11,13},{4,-20,13},{-1,6,14},{10,3,-18},{3,13,7},{-1,-17,9},{16,-9,4},{13,2,-9}], element(2, solve_sat([{-1,-15,20},{11,10,9},{-12,-5,16},{-2,14,-15},{-9,-11,14},{-18,12,2},{12,-6,20},{8,-12,-16},{17,-15,1},{-14,6,10},{13,15,14},{-6,-9,-5},{12,14,17},{8,19,3},{1,-8,-19},{-11,-2,1},{-1,-19,8},{-2,-1,-7},{-16,20,13},{-20,-19,4},{-9,-16,1},{7,-1,-6},{10,11,-5},{6,-15,7},{7,-13,10},{18,8,-13},{8,11,-19},{-6,18,-5},{-6,16,-12},{-20,-14,-1},{4,10,1},{-15,14,7},{-15,4,-18},{8,3,-17},{13,-3,1},{4,-13,-7},{16,-10,-5},{3,-1,-6},{-20,8,-18},{-9,-15,7},{2,-1,-8},{-12,20,-6},{-18,11,13},{4,-20,13},{-1,6,14},{10,3,-18},{3,13,7},{-1,-17,9},{16,-9,4},{13,2,-9}]))))}
    ].

unsat_test_() ->
    [{timeout, 10000, ?_assertEqual(unsat, solve_sat([{1, 1, 1}, {-1, -1, -1}]))},
    {timeout, 10000, ?_assertEqual(unsat, solve_sat([{16,11,-5},{-13,-18,-4},{-7,-4,-10},{9,-14,18},{-13,15,2},{17,10,-8},{2,-20,10},{8,1,-3},{9,3,15},{-20,1,5},{-15,-7,-6},{-17,13,19},{1,-20,4},{9,5,-11},{-1,12,17},{17,9,-15},{4,-9,3},{3,-14,13},{5,-3,12},{12,18,6},{10,-12,-2},{-14,16,4},{10,6,2},{-2,14,-9},{-1,2,8},{17,-12,5},{-13,-16,-7},{-9,13,16},{-9,19,6},{-6,-10,-8},{14,9,-12},{16,7,4},{-13,-5,4},{-3,-20,14},{-14,16,-7},{-5,14,-7},{-14,15,-2},{16,9,10},{1,9,4},{13,12,9},{-9,8,-14},{13,4,6},{20,-11,2},{-11,-12,18},{-13,15,-9},{10,8,-16},{-6,16,3},{16,1,20},{3,-6,12},{-15,1,18},{4,-19,-5},{-6,-15,4},{15,-11,-20},{15,-6,19},{6,-14,16},{-10,-5,-12},{-17,-6,12},{1,10,3},{-11,-16,17},{-11,4,-6},{12,-9,5},{-2,14,5},{-20,-9,-16},{3,-7,-11},{-11,-14,6},{-14,7,-18},{19,13,2},{-18,-16,11},{9,11,-4},{16,7,-14},{-20,-6,16},{-13,-2,19},{13,-1,18},{4,20,1},{7,-18,-3},{9,-5,15},{-4,-7,17},{-7,-2,-8},{20,-3,-8},{-5,-3,10},{-18,-9,20},{17,-1,10},{18,14,-17},{18,-15,-20},{-18,-11,19},{-17,19,-10},{11,7,8},{-11,-3,-14},{-11,20,14},{-7,-16,-12},{-2,-7,3},{-13,-11,-9},{15,-20,-14},{6,-2,1},{-10,-17,-5},{19,13,10},{-17,4,9},{19,-10,-9},{-8,-19,-20},{-2,17,20},{14,-18,-17},{5,-11,-6},{2,-4,14},{-15,-17,10},{-10,-12,3},{3,9,15},{17,-12,-1},{-12,-7,2},{-17,-11,6},{16,2,5},{11,13,15},{-7,-8,6},{6,-11,8},{-6,-12,1},{-13,-15,-10},{-15,-2,-17},{1,11,14},{-13,10,-4},{-17,5,12},{-17,-9,18},{16,19,-3},{16,15,19},{-17,4,-12},{-16,18,14},{-15,20,2},{12,17,-14},{5,14,-3},{-2,-4,-7},{-3,-10,9},{-5,-2,9},{15,8,10},{13,-3,-6},{11,7,13},{-5,19,1},{-4,16,-10},{10,-7,-20},{1,13,-10},{-17,-2,-20},{-17,18,-20},{-8,-13,3},{19,-4,18},{-16,-7,12},{-18,-3,-7},{-3,-19,-20},{-9,13,10},{-19,-3,-18},{11,14,6},{20,-6,14},{4,16,-7},{-20,5,2},{6,-1,16},{-10,1,-19},{-2,-9,18},{-6,-12,10},{-1,-11,5},{2,15,17},{-4,-14,19},{-4,2,-18},{5,11,7},{-8,5,16},{-14,2,17},{20,1,-18},{-14,20,-9},{-13,-5,11},{18,-6,-11},{6,18,11},{10,-19,-12},{-8,-15,3},{6,-10,16},{18,2,20},{-14,9,16},{17,8,12},{-15,-18,20},{8,5,-10},{9,-19,8},{6,-18,-20},{-16,-9,-6},{-13,-8,-12},{16,-11,-18},{10,-2,1},{7,-8,-5},{2,-11,-16},{4,-9,-5},{-19,-2,18},{-10,3,14},{-7,12,-4},{1,9,11},{8,18,4},{-12,6,-10},{11,7,-6},{-9,13,-7},{8,-2,9},{12,-13,-15},{-13,-15,-20},{6,2,3},{-19,1,2},{-5,-4,-8},{-4,-19,-16},{-5,2,-7},{-2,8,11}]))}
    ].


%%%===================================================================
%%% Property-based Test
%%%===================================================================

%%% Property: Length of result (if sat) is equal to number of variables in instance.
prop_length_sat() ->
    ?FORALL(Clause, generate_instance(), check_length_sat(Clause, solve_sat(Clause))).

%%% Property: Result (if sat) satisfy the instance.
prop_check_result_sat() ->
    ?FORALL(Clause, generate_instance(), check_result(Clause, solve_sat(Clause))).


%%%===================================================================
%%% Helper function for property-base test.
%%%===================================================================
check_length_sat(Clause, Result) ->
    case Result =:= unsat of
        true -> true;
        false ->
            {sat, List} = Result,
            element(1, number_of_variables(Clause)) =:= length(List)
    end.


%% --------------------------------------------------------------------
%% Function: generate_instance/0
%% Description: generate instance of [1..20] variables, [1..100] tuples 
%% Return: instance of [1..20] variables, [1..100] tuples
%% --------------------------------------------------------------------
generate_instance() ->
    Number_Of_Variable = rand:uniform(20),
    Number_Of_Tuple = rand:uniform(100),
    generate_instance_helper(Number_Of_Tuple, Number_Of_Variable, []).


generate_instance_helper(Number_Of_Tuple, _, Result) when Number_Of_Tuple =< 0 -> Result;
generate_instance_helper(Number_Of_Tuple, Number_Of_Variable, Result) ->
    Variable1 = generate_random_variable(Number_Of_Variable),
    Variable2 = generate_random_variable(Number_Of_Variable),
    Variable3 = generate_random_variable(Number_Of_Variable),
    generate_instance_helper(Number_Of_Tuple - 1, Number_Of_Variable, [{Variable1, Variable2, Variable3}|Result]).


%% --------------------------------------------------------------------
%% Function: generate_random_variable/1
%% Description: generate random number in [-Max, Max] 
%% Return: instance of [1..20] variables, [1..100] tuples
%% --------------------------------------------------------------------
generate_random_variable(Max) ->
    Value = rand:uniform(Max),
    Sign = rand:uniform(2),
    case Sign =:= 1 of 
        true -> Value;
        false -> -Value
    end.    


%% --------------------------------------------------------------------
%% Function: check_result/2
%% Description: Check Result against Clause 
%% Return:  - true if Result = unsat, or Result satisfies the Clause
%%          - false otherwise
%% --------------------------------------------------------------------
check_result(Clause, Result) ->
    case Result =:= unsat of 
        true -> true;
        false ->
            {sat, List} = Result,
            validate_clause(Clause, List)
    end.


%%%===================================================================
%%% Main function
%%%===================================================================

%% --------------------------------------------------------------------
%% Function: solve_sat/1
%% Description: evaluate 3-SAT instance
%% Return: - {sat,[Var1,Var2,...]}, where Var1, Var2, ... are boolean 
%%              values (true or false) that make the formula
%%              satisfiable when assigned to the respective variables.
%%         - unsat: if the formula is not satisfiable. 
%% --------------------------------------------------------------------
-spec solve_sat([{integer(), integer(), integer()}]) -> tuple().
solve_sat(Clause) ->
    {Variables, ElementGraph} = number_of_variables(Clause),
    solve_sat_helper(Clause, 1, Variables, maps:new(), ElementGraph).


solve_sat_helper(Clause, I, Variables, ValueGraph, ElementGraph) ->
    case I > Variables of
        true -> 
            Result = check_clause(Clause, ValueGraph),
            case Result of 
                true -> {sat, graph_to_list(ValueGraph, Variables)};
                false -> unsat
            end;
        false ->
           case maps:is_key(I, ElementGraph) of 
               true -> 
                    case I =< 5 of
                        true ->
                            Par = self(),
                            spawn_link(fun () -> 
                                Par ! solve_sat_helper(Clause, I + 1, Variables, maps:put(I, false, maps:put(-I, true, ValueGraph)), ElementGraph)           % calculate max_cycle(N, C) in other process.
                            end),

                            Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                            Result_False = receive Result -> Result end,

                            case Result_True =:= unsat of 
                                true -> 
                                    Result_False;    
                                
                                false ->
                                    Result_True
                            end;

                        false ->
                            Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                            case Result_True =:= unsat of 
                                true -> 
                                    Result_False = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, false, maps:put(-I, true, ValueGraph)), ElementGraph),
                                    Result_False;    
                                
                                false ->
                                    Result_True

                            end
                    end;
                false ->
                    Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                    Result_True
            end
    end.




%%%===================================================================
%%% Helper functions for main function
%%%===================================================================
-spec max(list(T)) -> T.
max([]) -> 0;
max([Max|T]) -> max(Max, max(T)).

number_of_variables(List) -> number_of_variables(List, 0, maps:new()).
number_of_variables([], Result, Graph) -> {Result, Graph};
number_of_variables([{X, Y, Z}|Tail], Result, Graph) -> 
    Graph_X = maps:put(abs(X), 0, Graph),
    Graph_Y = maps:put(abs(Y), 0, Graph_X),
    Graph_Z = maps:put(abs(Z), 0, Graph_Y),
    number_of_variables(Tail, max([abs(X), abs(Y), abs(Z), Result]), Graph_Z).

-spec check_clause([{integer(), integer(), integer()}], _) -> boolean.
check_clause([], _) -> true;
check_clause([{X,Y,Z}|Tail], Graph) ->
    {ok, X_Boolean} = maps:find(X, Graph),
    {ok, Y_Boolean} = maps:find(Y, Graph),
    {ok, Z_Boolean} = maps:find(Z, Graph),
    (X_Boolean or Y_Boolean or Z_Boolean) and check_clause(Tail, Graph).

graph_to_list(Graph, Variables) -> graph_to_list(Graph, Variables, Variables, []).
graph_to_list(Graph, Variables, I, Result) ->
    case I =:= 0 of 
        true -> Result;
        false -> 
            {ok, Value} = maps:find(I, Graph),
            graph_to_list(Graph, Variables, I - 1, [Value|Result]) 
    end.


list_to_graph(List) -> list_to_graph(List, maps:new(), 1).

list_to_graph([], Graph, _) -> Graph;

list_to_graph([Head|Tail], Graph, I) ->
    G1 = maps:put(I, Head, Graph),
    G2 = maps:put(-I, not Head, G1),
    list_to_graph(Tail, G2, I + 1).


validate_clause(Clause, List) ->
    Graph = list_to_graph(List),
    check_clause(Clause, Graph).
