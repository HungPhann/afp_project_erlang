%%%-------------------------------------------------------------------
%%% @author Phan Huy Hung <Huyhung.Phan.0630@student.uu.se>
%%% @doc This module defines a server process that
%%%         evaluate 3-SAT instance.
%%%
%%% @end
%%%-------------------------------------------------------------------


-module(sat_server).
-behaviour(gen_server).

%% API
-export([
    start_link/1,
    start_link/0,
    stop/0
    ]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,terminate/2, code_change/3]).

-define(SERVER, ?MODULE).
-define(DEFAULT_PORT, 3547).
-record(state, {port, lsock, connection=0}).
-record(process_state, {socket, child_pid=undefined, available=true, previous_message=undefined, previous_result=ready, count=1}).


%%%===================================================================
%%% API
%%%===================================================================

start_link(Port) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [Port], []).

start_link() ->
    start_link(?DEFAULT_PORT).

stop() ->
    gen_server:cast(?SERVER, stop).



%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([Port]) ->
    Pid = spawn(fun() -> server(0) end),
    register(state_server, Pid),
    {ok, LSock} = gen_tcp:listen(Port, [{active, true},
                                        {keepalive, true},
                                        {reuseaddr, true}]),

    spawn(fun() -> parallel_connection(LSock) end),
    {ok, #state{port = Port, lsock = LSock}, 0}.


handle_call(_, _From, State) ->
    {reply, {ok, ok, State}}.

handle_cast(stop, State) ->
    {stop, normal, State}.


handle_info({tcp, Socket, RawData}, State) ->
    gen_tcp:send(Socket, io_lib:fwrite("~p~n", [RawData])),
    {noreply, State};

handle_info({tcp_closed, _Socket}, #state{lsock = LSock} = State) ->
    {ok, _Sock} = gen_tcp:accept(LSock),
    {noreply, State};
    % {stop, normal, State};

handle_info(timeout, State) ->
    % {ok, _Sock} = gen_tcp:accept(LSock),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%%%===================================================================
%%% intenal functions
%%%===================================================================

parallel_connection(Listen) ->
    {ok, Socket} = gen_tcp:accept(Listen),

    NumberOfConnections = get_connection(state_server),
    % gen_tcp:send(Socket, io_lib:fwrite("Number of Connection: ~p~n", [NumberOfConnections])),
	
    case NumberOfConnections < 8 of 
        true -> 
            increase_connection(state_server),
            gen_tcp:send(Socket, io_lib:fwrite("~p~n", [ready])),
            spawn(fun() -> parallel_connection(Listen) end),
            conn_loop(#process_state{socket=Socket});
        false -> 
            gen_tcp:send(Socket, io_lib:fwrite("~p~n", [busy])),
            gen_tcp:close(Socket),
            parallel_connection(Listen)
    end.




conn_loop(Process_State) ->
    receive
	{tcp, Socket, RawData} ->
        % gen_tcp:send(Socket, io_lib:fwrite("~p~n", [RawData])),
        case RawData =:= [4] of                                                                                                         % exit value
            true -> 
                handle_close_connection(Process_State);
            false ->                                                                                                
                try
                    Previous_message = Process_State#process_state.previous_message,
                    case string:substr(RawData, string:len(RawData) - 1, string:len(RawData)) =:= "\r\n" of 
                        false ->                                                                                                        % packet is incomplete
                            case Previous_message =:= undefined of 
                                true -> conn_loop(Process_State#process_state{previous_message=RawData});
                                false -> 
                                    New_message = Previous_message ++ RawData,                                                          % concatinate to previous message
                                    conn_loop(Process_State#process_state{previous_message=New_message})
                            end;
                        true ->                                                                                                         % packet is now completed
                            Message = case Previous_message =:= undefined of                                                            % Get complete message
                                        true -> RawData;
                                        false -> Previous_message ++ RawData
                                    end,
                            Input = convert(Message),                                                                                   % convert message to Erlang expression
                            case is_atom(Input) and (Input =:= abort) of                                                        
                                true ->                                                                                                 % Input is abort
                                    gen_tcp:send(Socket, io_lib:fwrite("~p~n", [aborted])),          
                                    case Process_State#process_state.child_pid /= undefined of 
                                        true ->                                                                                         % Server is evaluating a 3-sat
                                            Process_State#process_state.child_pid ! {stop, self()},                                     % Stop evaluating
                                            conn_loop(Process_State#process_state{child_pid=undefined, available=true, previous_message=undefined, previous_result=aborted, count=1});                 % Continue to receive request with State = available    
                                        false ->
                                            conn_loop(Process_State#process_state{previous_message=undefined, previous_result=aborted, count=1})                          % Continue to receive request with same State, otherwise
                                    end;
                                false ->                                                                                                 
                                    case Process_State#process_state.available of 
                                        true ->                                                                                         % Server is available   
                                            case is_valid_CNF(Input) of 
                                                true ->                                                                                 % valid SAT form 
                                                    gen_tcp:send(Socket, io_lib:fwrite("~p~n", [trying])),                              % First 'trying'
                                                    Self_Pid = self(),                                                                              
                                                    Pid = spawn(fun() -> process1(Self_Pid, Socket, Input) end),                        % spawn new process for calculating
                                                    conn_loop(Process_State#process_state{child_pid=Pid, available=false, previous_message=undefined, count=1});             % Set server is unavailable and continue to receive request
                                                false ->                                                                                % invalid SAT form
                                                    gen_tcp:send(Socket, io_lib:fwrite("~p~n", [ignored])),                             % ignored
                                                    conn_loop(Process_State#process_state{previous_message=undefined, previous_result=ignored, count=1})                                                            % Continue to receive request with same State
                                            end;
                                        false ->
                                            gen_tcp:send(Socket, io_lib:fwrite("~p~n", [ignored])),                                     % 'ignored' when server is unavailable
                                            conn_loop(Process_State#process_state{previous_message=undefined, previous_result=ignored,count=1})                                                                    % Continue to receive request with same State
                                    end
                            end
                        end
                catch 
                    _ -> 
                        gen_tcp:send(Socket, io_lib:fwrite("~p~n", [ignored])),                                                 % Cannot parse input
                        conn_loop(Process_State#process_state{previous_message=undefined, previous_result=ignored,count=1})
                end
        end;
        
                    

	{tcp_closed, _} ->                                                                                             % connection is closed
        handle_close_connection(Process_State);

    
    {set_available, _, Result} ->                                                                                               % Set server is available
        conn_loop(Process_State#process_state{available=true, child_pid=undefined, previous_result=Result,count=1})                                         

    after 10000 ->
        case Process_State#process_state.available of 
            true ->
                Count = Process_State#process_state.count,
                case Count >= 6 of 
                    true ->                                                                                                     % timeout if server remains available after 1 min
                        decrease_connection(state_server),
                        gen_tcp:send(Process_State#process_state.socket, io_lib:fwrite("~p~n", [timeout])),
                        gen_tcp:close(Process_State#process_state.socket);
                    false ->
                        gen_tcp:send(Process_State#process_state.socket, io_lib:fwrite("~p~n", [Process_State#process_state.previous_result])),
                        conn_loop(Process_State#process_state{count=Count+1})
                end;
            false ->
                conn_loop(Process_State#process_state{count=1})
        end

    end.

handle_close_connection(Process_State) ->
    decrease_connection(state_server),                                                                              % Change number of connections
    case Process_State#process_state.child_pid /= undefined of 
        true ->
            Process_State#process_state.child_pid ! {stop, self()},                                                 % Stop evaluating if any
            ok;
        false ->
            ok
    end.


process1(Parent, Socket, Clause) ->
    Self_Pid = self(),
    Pid = spawn(fun() -> process2(Clause, Self_Pid) end),                                                                         % spawn new process for evaluating 
    process1_helper(Parent, Socket, Pid, 1).


process1_helper(Parent, Socket, Child_Pid, Time) ->
    receive 
        {result_reply, Result} ->                                                                                       % receive Result -> set server available and display Result to user
            Parent ! {set_available, self(), Result},                                                
            gen_tcp:send(Socket, io_lib:fwrite("~p~n", [Result]));

        {stop, _} ->                                                                                                    % receive stop signal from server    
            exit(Child_Pid, kill),                                                                                      % stop evaluating
            ok

    after 10000 ->                                                                                                      
        case Time >= 12 of                                                                                              % abort after 2 min                                                       
            true ->
                Parent ! {set_available, self(), aborted},                                                                       % set server available
                exit(Child_Pid, kill),                                                                                  % stop evaluating
                gen_tcp:send(Socket, io_lib:fwrite("~p~n", [aborted]));
            false ->
                gen_tcp:send(Socket, io_lib:fwrite("~p~n", [trying])),                                                  % every 10sec, 'trying'
                process1_helper(Parent, Socket, Child_Pid, Time + 1)
        end
    end.


process2(Clause, Parent) ->                                                                                                     % Function used for evaluating in new process
    Result = solve_sat(Clause),
    Parent ! {result_reply, Result}.    

%%%======================
%%% state_server to keep track of number of connections. 
%%%======================
server(NumberOfConnections) ->
    receive
        {report, Sender} ->
            Sender ! {report_reply, self(), NumberOfConnections},
            server(NumberOfConnections);
        {increase, Sender} ->
            Sender ! {increase_reply, self(), ok},
            server(NumberOfConnections + 1);
        {decrease, Sender} ->
            Sender ! {decrease_reply, self(), ok},
            server(NumberOfConnections - 1);
        % loop with new state!
        stop ->
            ok
    end.


%%% helper functions to interact with state_server.
get_connection(ServerPid) ->
    ServerPid ! {report, self()},
    receive
        {report_reply, _, NumberOfConnections} -> NumberOfConnections
    end.

increase_connection(ServerPid) ->
    ServerPid ! {increase, self()}.

decrease_connection(ServerPid) ->
    ServerPid ! {decrease, self()}.

%%%=====================================================================


convert(S) -> 
    {ok, Ts, _} = erl_scan:string(S), 
    Value = erl_parse:parse_term(Ts ++ [{dot,1}]), 
    case element(1, Value) =:= ok of 
        true -> element(2, Value);
        false -> throw(cannot_parse_argument_error)
    end.


-spec is_valid_clause(tuple()) -> boolean.
is_valid_clause({X, Y, Z}) ->
    is_integer(X) and is_integer(Y) and is_integer(Z);
is_valid_clause(_) ->
    false.

-spec is_valid_CNF(list()) -> boolean.
is_valid_CNF([Head|Tail]) ->
    is_valid_clause(Head) and is_valid_CNF(Tail);
is_valid_CNF([]) -> true;
is_valid_CNF(_) -> false.


-spec max(list(T)) -> T.
max([]) -> 0;
max([Max|T]) -> max(Max, max(T)).

-spec number_of_variables(list()) -> integer().
number_of_variables(List) -> number_of_variables(List, 0, maps:new()).

number_of_variables([], Result, Graph) -> {Result, Graph};
number_of_variables([{X, Y, Z}|Tail], Result, Graph) -> 
    Graph_X = maps:put(abs(X), 0, Graph),
    Graph_Y = maps:put(abs(Y), 0, Graph_X),
    Graph_Z = maps:put(abs(Z), 0, Graph_Y),
    number_of_variables(Tail, max([abs(X), abs(Y), abs(Z), Result]), Graph_Z).

-spec check_clause([{integer(), integer(), integer()}], _) -> boolean.
check_clause([], _) -> true;
check_clause([{X,Y,Z}|Tail], Graph) ->
    {ok, X_Boolean} = maps:find(X, Graph),
    {ok, Y_Boolean} = maps:find(Y, Graph),
    {ok, Z_Boolean} = maps:find(Z, Graph),
    (X_Boolean or Y_Boolean or Z_Boolean) and check_clause(Tail, Graph).

graph_to_list(Graph, Variables) -> graph_to_list(Graph, Variables, Variables, []).
graph_to_list(Graph, Variables, I, Result) ->
    case I =:= 0 of 
        true -> Result;
        false -> 
            {ok, Value} = maps:find(I, Graph),
            graph_to_list(Graph, Variables, I - 1, [Value|Result]) 
    end.


-spec solve_sat([{integer(), integer(), integer()}]) -> tuple().
solve_sat(Clause) ->
    {Variables, ElementGraph} = number_of_variables(Clause),
    solve_sat_helper(Clause, 1, Variables, maps:new(), ElementGraph).


solve_sat_helper(Clause, I, Variables, ValueGraph, ElementGraph) ->
    case I > Variables of
        true -> 
            Result = check_clause(Clause, ValueGraph),
            case Result of 
                true -> {sat, graph_to_list(ValueGraph, Variables)};
                false -> unsat
            end;
        false ->
           case maps:is_key(I, ElementGraph) of 
               true -> 
                    case I =< 5 of
                        true ->
                            Par = self(),
                            spawn_link(fun () -> 
                                Par ! solve_sat_helper(Clause, I + 1, Variables, maps:put(I, false, maps:put(-I, true, ValueGraph)), ElementGraph)           % calculate max_cycle(N, C) in other process.
                            end),

                            Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                            Result_False = receive Result -> Result end,

                            case Result_True =:= unsat of 
                                true -> 
                                    Result_False;    
                                
                                false ->
                                    Result_True
                            end;

                        false ->
                            Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                            case Result_True =:= unsat of 
                                true -> 
                                    Result_False = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, false, maps:put(-I, true, ValueGraph)), ElementGraph),
                                    Result_False;    
                                
                                false ->
                                    Result_True

                            end
                    end;
                false ->
                    Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                    Result_True
            end
    end.
