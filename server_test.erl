-module(server_test).
-export([]).

-include_lib("eunit/include/eunit.hrl").


%%%===================================================================
%%% Test
%%%===================================================================

%%% test if server can start successfully.
setup_test_() ->
    [?_assertMatch({ok, _}, sat_server:start_link())].

%%% test if server accept maximum 8 connection.
limited_connection_test_() ->
    [?_assertEqual(true, limited_connection_test_helper())].

%%% test if server sends timeout and close connection after 60s of idle.
timeout_test_() ->
    [{timeout, 60000, ?_assertEqual(true, timeout_test_helper())}].


%%%===================================================================
%%% Helper function
%%%===================================================================
%% --------------------------------------------------------------------
%% Function: connect/0
%% Description: Connect to "localhost 3547"
%% Return: {Socket, Response}
%%      where: Response is the first message server sends back after successful connection. 
%%              Response = {error, timeout} if server doesn't sends back anything after 1s
%% --------------------------------------------------------------------
connect() ->
    {ok, Socket} = gen_tcp:connect("localhost", 3547, [{packet, 0}]),
    Response = receive
        {tcp, _, RawData} ->
            RawData
        
        after 1000 ->
            {error, timeout}
    end,
    {Socket, Response}.


%% --------------------------------------------------------------------
%% Function: connection_is_close/1
%% Description: Check whether Socket is closed or not.
%% Return: true if Socket is closed, false otherwise
%% --------------------------------------------------------------------
connection_is_closed(Socket) ->
    Result = gen_tcp:send(Socket, ""),
    is_tuple(Result) and (Result =:= {error, closed}).


%% --------------------------------------------------------------------
%% Function: close_all_connection/1
%% Description: Close all connection in Socket list.
%% Return: ok
%% --------------------------------------------------------------------
close_all_connection([]) -> ok;
close_all_connection([Socket|Sockets]) ->
    gen_tcp:close(Socket),
    close_all_connection(Sockets).


%% --------------------------------------------------------------------
%% Function: limited_connection_helper/0
%% Description: Send 9 request to connect to "localhost 3547"
%%              First 8 connections should be successful and get "ready\n" in reply
%%              9th connection should get "busy\n in reply" and be closed after that.
%% Return: true if description is satisfied, false otherwise.
%% --------------------------------------------------------------------
limited_connection_test_helper() -> limited_connection_test_helper(1, []).

limited_connection_test_helper(I, Sockets) when I =< 8 ->
    {Socket, Response} = connect(),
    io:fwrite("~p~n", [Response]),
    case "ready\n" =:= Response of 
        true -> limited_connection_test_helper(I + 1, [Socket|Sockets]);
        false -> false
    end;

limited_connection_test_helper(_, Sockets) ->
    {Socket, Response} = connect(),
    io:fwrite("~p~n", [Response]),
    Result = ("busy\n" =:= Response) and connection_is_closed(Socket),
    close_all_connection([Socket|Sockets]),
    Result.


%% --------------------------------------------------------------------
%% Function: timeout_get_message/0
%% Description: Continuously get message sent from server until get "timeout\n"
%% Return: "timeout\n" when get message is "timeout\n"
%%          {error, timeout} when cannot receive anything from server after 1s.
%% --------------------------------------------------------------------
timeout_get_message() ->
    receive 
        {tcp, _, RawData} ->
            case RawData =:= "timeout\n" of 
                true -> RawData;
                false -> timeout_get_message()
            end

        after 1000 ->
            {error, timeout}
    end.


%% --------------------------------------------------------------------
%% Function: timeout_test_helper/0
%% Description: Connect to "localhost 3547" and wait for 60s.
%%              Check if server sends timeout message and close connection.
%% Return: true if server sends timeout message and close connection,
%%          false otherwise
%% --------------------------------------------------------------------
timeout_test_helper() ->
    {Socket, _} = connect(),
    timer:sleep(60000),
    Message = timeout_get_message(),
    io:fwrite("Message: ~p~n", [Message]),
    io:fwrite("Connection is closed: ~p~n", [connection_is_closed(Socket)]),
    Result = ((Message =:= "timeout\n") and (connection_is_closed(Socket))),
    close_all_connection([Socket]),
    Result.


