-module(sat).
-export([is_valid_CNF/1, solve_sat/1]).


-spec is_valid_clause(tuple()) -> boolean.
is_valid_clause({X, Y, Z}) ->
    is_integer(X) and is_integer(Y) and is_integer(Z);
is_valid_clause(_) ->
    false.

-spec is_valid_CNF(list()) -> boolean.
is_valid_CNF([Head|Tail]) ->
    is_valid_clause(Head) and is_valid_CNF(Tail);
is_valid_CNF([]) -> true;
is_valid_CNF(_) -> false.


-spec max(list(T)) -> T.
max([]) -> 0;
max([Max|T]) -> max(Max, max(T)).

-spec number_of_variables(list()) -> integer().
number_of_variables(List) -> number_of_variables(List, 0, maps:new()).

number_of_variables([], Result, Graph) -> {Result, Graph};
number_of_variables([{X, Y, Z}|Tail], Result, Graph) -> 
    Graph_X = maps:put(abs(X), 0, Graph),
    Graph_Y = maps:put(abs(Y), 0, Graph_X),
    Graph_Z = maps:put(abs(Z), 0, Graph_Y),
    number_of_variables(Tail, max([abs(X), abs(Y), abs(Z), Result]), Graph_Z).

-spec check_clause([{integer(), integer(), integer()}], _) -> boolean.
check_clause([], _) -> true;
check_clause([{X,Y,Z}|Tail], Graph) ->
    {ok, X_Boolean} = maps:find(X, Graph),
    {ok, Y_Boolean} = maps:find(Y, Graph),
    {ok, Z_Boolean} = maps:find(Z, Graph),
    (X_Boolean or Y_Boolean or Z_Boolean) and check_clause(Tail, Graph).

graph_to_list(Graph, Variables) -> graph_to_list(Graph, Variables, Variables, []).
graph_to_list(Graph, Variables, I, Result) ->
    case I =:= 0 of 
        true -> Result;
        false -> 
            {ok, Value} = maps:find(I, Graph),
            graph_to_list(Graph, Variables, I - 1, [Value|Result]) 
    end.


-spec solve_sat([{integer(), integer(), integer()}]) -> tuple().
solve_sat(Clause) ->
    {Variables, ElementGraph} = number_of_variables(Clause),
    solve_sat_helper(Clause, 1, Variables, maps:new(), ElementGraph).


solve_sat_helper(Clause, I, Variables, ValueGraph, ElementGraph) ->
    case I > Variables of
        true -> 
            Result = check_clause(Clause, ValueGraph),
            case Result of 
                true -> {sat, graph_to_list(ValueGraph, Variables)};
                false -> unsat
            end;
        false ->
           case maps:is_key(I, ElementGraph) of 
               true -> 
                    case I =< 5 of
                        true ->
                            Par = self(),
                            spawn_link(fun () -> 
                                Par ! solve_sat_helper(Clause, I + 1, Variables, maps:put(I, false, maps:put(-I, true, ValueGraph)), ElementGraph)           % calculate max_cycle(N, C) in other process.
                            end),

                            Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                            Result_False = receive Result -> Result end,

                            case Result_True =:= unsat of 
                                true -> 
                                    Result_False;    
                                
                                false ->
                                    Result_True
                            end;

                        false ->
                            Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                            case Result_True =:= unsat of 
                                true -> 
                                    Result_False = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, false, maps:put(-I, true, ValueGraph)), ElementGraph),
                                    Result_False;    
                                
                                false ->
                                    Result_True

                            end
                    end;
                false ->
                    Result_True = solve_sat_helper(Clause, I + 1, Variables, maps:put(I, true, maps:put(-I, false, ValueGraph)), ElementGraph),
                    Result_True
            end
    end.



% -spec check_clause([{integer(), integer(), integer()}], _) -> boolean.
% check_clause([], _) -> true;
% check_clause([{X,Y,Z}|Tail], List) ->
%     X_Boolean = case X > 0 of 
%                     true -> lists:nth(X, List);
%                     false -> not (lists:nth(-X, List))
%                 end,
%     Y_Boolean = case Y > 0 of 
%                     true -> lists:nth(Y, List);
%                     false -> not (lists:nth(-Y, List))
%                 end,
%     Z_Boolean = case Z > 0 of 
%                     true -> lists:nth(Z, List);
%                     false -> not (lists:nth(-Z, List))
%                 end,
%     (X_Boolean or Y_Boolean or Z_Boolean) and check_clause(Tail, List).



% -spec solve_sat([{integer(), integer(), integer()}]) -> tuple().
% solve_sat(Clause) ->
%     Variables = number_of_variables(Clause),
%     solve_sat_helper(Clause, 1, Variables, []).


% solve_sat_helper(Clause, I, Variables, List) ->
%     case I > Variables of
%         true -> 
%             Result = check_clause(Clause, List),
%             case Result of 
%                 true -> {sat, List};
%                 false -> {unsat}
%             end;
%         false -> 
%            Result_True = solve_sat_helper(Clause, I + 1, Variables, List ++ [true]),
%            case element(1, Result_True) =:= sat of 
%                true -> Result_True;
%                false ->
%                    Result_False = solve_sat_helper(Clause, I + 1, Variables, List ++ [false]),
%                    Result_False
%             end
%         end.




